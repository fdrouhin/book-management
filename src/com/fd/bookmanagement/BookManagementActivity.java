package com.fd.bookmanagement;

import java.util.StringTokenizer;

import android.app.ListActivity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.fd.bookmanagement.books.Book;
import com.fd.bookmanagement.isbnrequests.ISBNDBRequest;

/**
 * Book management
 * Todo List:
 * <li>retrieve book cover
 * <li>Search activity ISBNDB, Amazon, Google book in book creation
 * <li>Language management => done ?
 * <li>tablet, screen, orientation management
 * <li>GridLayout for buttons
 * <li>Import and export from SQLLite local database
 * <li>Settings : clear history, change book ISBN, access key for different ISBN provider, help web page
 * <li>Manage authors in search activity separated by , to add each author independently
 * <li> Find a correct to transmit a bitmap from an activity to another
 * <li>Redo the layout of the main activity to use a Linear Layout ? is it usefull ?
 * <li>BookManagementActivity#manageQueryString to be done 
 * <li>Check simpleFormatDate warning to be solved
 * <li>Remove my access key
 * <li>Add new window to set the access key
 * <li>Test to be done
 * 		<li> cancel the operation during web search
 * 		<li> bad web site => problem => async does not finished => should be debugged <==== seemes to be ok !
 * 		<li> bad ISBN request
 * <li>Add new gender in list
 * <li>Add gender to the list of gender in BookModificationActivity => ArrayAdapter#add
 * <li>Doc : long click to display the modification menu etc., simple click to display synopsis
 * <li>Bugs:
 * 		<li>Image of first element in ListAdapter => solved by setting the book cover to default but not a pleasant solution !
 * 		<li>Name of book when it's modified
 * @author Frederic.Drouhin
 *
 */
public class BookManagementActivity extends ListActivity {

	/**
	 * Can be set by ApplicationInfo.DEBUG
	 */
	public static boolean debug = true;

	/**
	 * For tagging message in log file
	 */
	public static final String TAG_LOG = "BookManagement::ErrorLog";

	/**
	 * Search suggestions see Searchable Android documentation
	 */
	public static SearchRecentSuggestions suggestions ;

	/**
	 * Library, a list of books
	 */
	public static LibraryAdapter libraryAdapter ;

	/**
	 * Book for transmission to another activities
	 * Is an elegant solution ? but solution less effort, no object copy and since the Bitmap failed to be Serializable and Parcealabe
	 */
	public static Book activityBook ;

	/**
	 * onCreate
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// My Access key
		ISBNDBRequest.accessKey = "56IXDMED" ; 

		// Disable the title show
		//ActionBar bar = getActionBar();
		//bar.setDisplayShowTitleEnabled(false);

		// for search bar in navigation bar
		handleIntent(getIntent());

		// Suggestion search
		suggestions = new SearchRecentSuggestions(this, TitleAuthorSuggestionProvider.AUTHORITY, TitleAuthorSuggestionProvider.MODE);

		// Adapter for ListView
		libraryAdapter = new LibraryAdapter(this);
		setListAdapter(libraryAdapter);

		// Utils
		Utils.context = this ;

		// Default cover & date format
		Book.defaultCover = BitmapFactory.decodeResource(this.getResources(), R.drawable.secret_book_128);
		Book.dateFormat = this.getResources().getString(R.string.date_format);

		// Debug is on ?  android:debuggable="true"
		if (ApplicationInfo.FLAG_DEBUGGABLE != 0) {
			debug = debug || true ;
		}
		
		// For debugging purpose, import a fake list 
		if (debug) importDataFromFile(null);
		if (debug) Toast.makeText(this,"Version Beta 0.3 mode debug on",Toast.LENGTH_LONG).show();
		if (debug) BookActivity.ISBNEXAMPLE = "9782744009228" ;

		// Try to adapt the image to screen size
		//Display display = getWindowManager().getDefaultDisplay();
		//Point size = new Point();
		//display.getSize(size);
		//if (size.x > 512) {
	}

	/* ---------------------------------------------------------------------------- */
	/* -------------------------- OPTIONS MENU ------------------------------------ */
	/* ---------------------------------------------------------------------------- */

	/**
	 * Options menu - create a search widget following the requirements provided by http://developer.android.com/guide/topics/search/search-dialog.html#UsingSearchWidget
	 * @param menu options menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the options menu from XML
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.options_menu, menu);

		// Get the SearchView and set the searchable configuration
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
		searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
		//searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default

		return true;
	}

	/**
	 *  Item management from options menu
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_search:
			onSearchRequested();
			return true;
		default:
			return false;
		}
	}

	/* ---------------------------------------------------------------------------- */
	/* -------------------------- SEARCH BAR AND ACTIVITY ------------------------- */
	/* ---------------------------------------------------------------------------- */
	/**
	 * 
	 */
	@Override
	protected void onNewIntent(Intent intent) {
		// Because this activity has set launchMode="singleTop", the system calls this method
		// to deliver the intent if this activity is currently the foreground activity when
		// invoked again (when the user executes a search from this activity, we don't create
		// a new instance of this activity, so the system delivers the search intent here)
		handleIntent(intent);
	}

	/**
	 * To handle the search activity
	 * @param intent
	 */
	private void handleIntent(Intent intent) {
		if (Intent.ACTION_VIEW.equals(intent.getAction())) {
			// handles a click on a search suggestion; launches activity to show word
			Intent wordIntent = new Intent(this, BookManagementActivity.class);
			wordIntent.setData(intent.getData());
			startActivity(wordIntent);
		} else if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			// handles a search query
			String query = intent.getStringExtra(SearchManager.QUERY);
			suggestions.saveRecentQuery(query, null);
			manageQuerySearch(query);			
		}
	}

	/**
	 * Manage the query from search bar
	 * @param query
	 */
	private void manageQuerySearch(String query) {
		Toast.makeText(BookManagementActivity.this,"handleIntent(" + query + ")",Toast.LENGTH_LONG).show();
	}

	/* ---------------------------------------------------------------------------- */
	/* -------------------------- IMPORTING DATA ---------------------------------- */
	/* ---------------------------------------------------------------------------- */
	/**
	 * import data from files
	 * @param filename file name for the import data from XML
	 */
	public void importDataFromFile (String filename) {

		// ------------------- For debugging purpose
		if (filename == null) {

			//String serie, int volume, String title, List<String> authors, String publisher, String isbn, String sourceInformation, Bitmap cover, String gender
			Book okko1 = new Book("Okko",1,"Le Cycle de l'eau - I", "Hub", "Delcourt", "2-84789-164-1", "http://www.bedetheque.com", BitmapFactory.decodeResource(this.getResources(), R.drawable.okkoeau1), this.getResources().getString(R.string.gender_bd)); 
			okko1.setCollection("Terres de L�gendes");
			okko1.setSynopsis("An 1108 du calendrier officiel de l'empire du Pajan. En cette p�riode tumultueuse, commun�ment appel�e l'�re Asagiri ou l'�re de la brume, les clans majeurs s'entre-d�chirent depuis des d�c�nnies pour s'emparer du pouvoir.Loin des champs de bataille, Okko, le r�nin sans ma�tre, est � la t�te d'un petit groupe de chasseurs de d�mons et arpente ainsi les terres de l'empire. Il est accompagn� de Noburo, singulier g�ant qui cache son identit� derri�re un masque rouge, et du moine Noshin, bonze fantasque et grand amateur de sak�. Ce dernier a la facult� de d'invoquer et de communiquer avec les forces de la nature.Tikku, jeune p�cheur, va faire appel � Okko pour retrouver Petite carpe, sa soeur enlev�e par un horde de pirates.Mais une telle mission a un prix... Elle entra�nera les quatre aventuriers bien plus loin qu'ils ne l'avaient imagin�...");
			libraryAdapter.add(okko1);
			Book okko2 = new Book("Okko",2,"Le Cycle de l'eau - II", "Hub", "Delcourt", "2-7560-0059-0", "http://www.bedetheque.com", BitmapFactory.decodeResource(this.getResources(), R.drawable.okkoeau2), this.getResources().getString(R.string.gender_bd)); 
			okko2.setCollection("Terres de L�gendes");
			okko2.setSynopsis("An 1108 du calendrier officiel de l'empire du Pajan. En cette p�riode tumultueuse, commun�ment appel�e l'�re Asagiri ou l'�re de la brume, les clans majeurs s'entre-d�chirent depuis des d�c�nnies pour s'emparer du pouvoir.Loin des champs de bataille, Okko, le r�nin sans ma�tre, est � la t�te d'un petit groupe de chasseurs de d�mons et arpente ainsi les terres de l'empire. Il est accompagn� de Noburo, singulier g�ant qui cache son identit� derri�re un masque rouge, et du moine Noshin, bonze fantasque et grand amateur de sak�. Ce dernier a la facult� de d'invoquer et de communiquer avec les forces de la nature.Tikku, jeune p�cheur, va faire appel � Okko pour retrouver Petite carpe, sa soeur enlev�e par un horde de pirates.Mais une telle mission a un prix... Elle entra�nera les quatre aventuriers bien plus loin qu'ils ne l'avaient imagin�...");
			libraryAdapter.add(okko2); 

			// For debugging purpose adding fake book
			String []myStringArray = { "Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune"};    
			for (String a: myStringArray) {

				Book b = new Book(a);
				b.setGender("Thriller");
				libraryAdapter.add(b);
			}			
		}

		// Update the suggestion search
		for (Book b: libraryAdapter.library) {
			suggestions.saveRecentQuery(b.getTitle().toLowerCase(), null);
			if (b.getAuthors() != null) {
				String authors = b.getAuthors() ;
				StringTokenizer token = new StringTokenizer(authors, ", ");
				while (token.hasMoreElements()) {
					suggestions.saveRecentQuery(token.nextToken().toString().toLowerCase(), null);				
				}
			}
		}
		
		// Sort the collection
		libraryAdapter.sort() ;
	}



	/* ---------------------------------------------------------------------------- */
	/* -------------------------- Click on a book --------------------------------- */
	/* ---------------------------------------------------------------------------- */

	/* (non-Javadoc)
	 * @see android.app.ListActivity#onListItemClick(android.widget.ListView, android.view.View, int, long)
	 */
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {

		Book book = libraryAdapter.get(position);

		Intent intent = new Intent(this,BookActivity.class);
		//intent.putExtra(BookActivity.TAGBOOKEXTRA, book);
		intent.putExtra(BookActivity.TAG_REQUEST_MODE, BookActivity.ACTIVITY_REQUESTCODE_BOOKDISPLAY);
		intent.putExtra(BookActivity.POSITION, position);
		activityBook = book ;
		startActivityForResult(intent, BookActivity.ACTIVITY_REQUESTCODE_BOOKDISPLAY);

		super.onListItemClick(l, v, position, id);
	}

	/* ---------------------------------------------------------------------------- */
	/* ------------------------------ Button management --------------------------- */
	/* ---------------------------------------------------------------------------- */

	/** 
	 * Create a new book
	 */
	public void bNewBook(View v) {
		Intent intent = new Intent(this,BookModificationActivity.class);
		//intent.putExtra(TAGBOOKEXTRA, book);
		intent.putExtra(BookActivity.TAG_REQUEST_MODE, BookActivity.ACTIVITY_REQUESTCODE_NEWBOOK);
		activityBook = null ;
		startActivityForResult(intent, BookActivity.ACTIVITY_REQUESTCODE_NEWBOOK);
	}

	/**
	 * Scan a book with ISBN
	 */
	public void bScanIsbn(View v) {
		Intent intent = new Intent(this,BookActivity.class);
		//intent.putExtra(TAGBOOKEXTRA, book);
		intent.putExtra(BookActivity.TAG_REQUEST_MODE, BookActivity.ACTIVITY_REQUESTCODE_BOOKSCAN);
		activityBook = null ;
		startActivityForResult(intent, BookActivity.ACTIVITY_REQUESTCODE_BOOKSCAN);
	}

	/**
	 * Result of the activities
	 */
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

		// New Book
		if (requestCode == BookActivity.ACTIVITY_REQUESTCODE_NEWBOOK) {

			if (resultCode == RESULT_OK && activityBook != null) {
				//Toast.makeText(this, "Book added", Toast.LENGTH_LONG).show();
				libraryAdapter.add(activityBook);
				libraryAdapter.notifyDataSetChanged();
				libraryAdapter.sort() ;
			}
			else {
				// Nothing
			}
		}
		// Scan book
		else if (requestCode == BookActivity.ACTIVITY_REQUESTCODE_BOOKSCAN) {

			if (resultCode == RESULT_OK && activityBook != null) {
				libraryAdapter.add(activityBook);
				libraryAdapter.notifyDataSetChanged();
				libraryAdapter.sort() ;
			}
			else {
				// Nothing
			}

		}
		// Book display and modification
		else if (requestCode == BookActivity.ACTIVITY_REQUESTCODE_BOOKDISPLAY) {

			if (resultCode == RESULT_OK && activityBook != null) {
				int position = intent.getIntExtra(BookActivity.POSITION, -1);
				if (position != -1) {
					libraryAdapter.set(position, activityBook);
					libraryAdapter.notifyDataSetChanged();
					libraryAdapter.sort() ;
				}
			}
			else {
				// Nothing
			}

		}

	}


	/* ---------------------------------------------------------------------------- */
	/* -------------------------- Status Bar -------------------------------------- */
	/* ---------------------------------------------------------------------------- */
}
