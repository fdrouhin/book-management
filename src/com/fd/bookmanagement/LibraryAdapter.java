package com.fd.bookmanagement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fd.bookmanagement.books.Book;


/**
 * The duplication of books is not managed here !
 * @author Frederic.Drouhin
 *
 */
public class LibraryAdapter extends BaseAdapter {

	/**
	 * List of books
	 */
	public List<Book> library = new ArrayList<Book>() ;

	/**
	 * Max character for listview // not anymore needed since the max number of lines is specified in the TextView
	 */
	//public static final int maxCharacterInLine = 40 ;
	
	/**
	 * Context 
	 */
	private Context context ;

	/**
	 * Create a library
	 */
	public LibraryAdapter(Context context) {

		this.context = context ;
	}
	
	/* --------------------------------------------------------------------------------------------- */
	/* ------------------- For the list of book : library ------------------------------------------ */
	/* --------------------------------------------------------------------------------------------- */
	
	/**
	 * @param location
	 * @param object
	 * @return
	 * @see java.util.List#set(int, java.lang.Object)
	 */
	public Book set(int location, Book object) {
		return library.set(location, object);
	}



	/**
	 * @param object
	 * @return
	 * @see java.util.Set#add(java.lang.Object)
	 */
	public boolean add(Book object) {
		return library.add(object);
	}

	/**
	 * 
	 * @see java.util.Set#clear()
	 */
	public void clear() {
		library.clear();
	}

	/**
	 * @param object
	 * @return
	 * @see java.util.Set#contains(java.lang.Object)
	 */
	public boolean contains(Object object) {
		return library.contains(object);
	}

	/**
	 * @param object
	 * @return
	 * @see java.util.Set#equals(java.lang.Object)
	 */
	public boolean equals(Object object) {
		return library.equals(object);
	}

	/**
	 * @return
	 * @see java.util.Set#isEmpty()
	 */
	public boolean isEmpty() {
		return library.isEmpty();
	}

	/**
	 * @param object
	 * @return
	 * @see java.util.Set#remove(java.lang.Object)
	 */
	public boolean remove(Object object) {
		return library.remove(object);
	}

	/**
	 * @return
	 * @see java.util.Set#size()
	 */
	public int size() {
		return library.size();
	}

	/**
	 * @param array
	 * @return
	 * @see java.util.Set#toArray(T[])
	 */
	public <T> T[] toArray(T[] array) {
		return library.toArray(array);
	}
	
	/**
	 * @param location location in the list
	 * @return book on location
	 * @see java.util.List#get(int)
	 */
	public Book get(int location) {
		return library.get(location);
	}

	/**
	 * Sort the library
	 */
	public void sort() {
		Collections.sort(this.library);
	}
	
	/* ------------------------------------------------------------------------------- */
	/* ------------------------------ Adapter methods -------------------------------- */
	/* ------------------------------------------------------------------------------- */

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		return library.size() ;
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int position) {
		return (Object)(library.get(position));
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int position) {
		return position;
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// convertView null ?
		ViewHolder holder = null ;
		
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(com.fd.bookmanagement.R.layout.libraryadapter_layout, null);
			
			holder = new ViewHolder() ;
			holder.textView = (TextView) convertView.findViewById(com.fd.bookmanagement.R.id.booktitleauthor);
			holder.imageView = (ImageView)convertView.findViewById(com.fd.bookmanagement.R.id.bookcover);
			
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder)convertView.getTag() ;
		}

		// R�cup�ration de l�item � la position
		Book val = library.get(position);

		// Mise en place de l�affichage avec un nombre de caract�re pour le textview cr�e
		holder.textView.setText(val.getTitleAuthor()) ; //LibraryAdapter.maxCharacterInLine));// not anymore needed since the max number of lines is specified in the TextView
		if (val.getCover() != null) {
			holder.imageView.setImageBitmap(val.getCover());
		}
		else {
			if (Book.defaultCover != null) {
				holder.imageView.setImageBitmap(Book.defaultCover);
			}
		}

		return convertView;
	}
	
	/**
	 * Just to avoid finding again the views needed to display
	 * @author Frederic.Drouhin
	 *
	 */
	private static class ViewHolder {
		public TextView textView ;
		public ImageView imageView ;
	}

}
