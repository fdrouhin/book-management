package com.fd.bookmanagement.isbnrequests;


public interface OnRequestCompleted {
	
	/**
	 * Listener that should be called when the http request is completed 
	 * @param responseString the answer from request
	 */
	public void onIsbnRequestCompleted(String responseString) ;
}
