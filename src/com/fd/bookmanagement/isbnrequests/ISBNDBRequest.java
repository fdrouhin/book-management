package com.fd.bookmanagement.isbnrequests;

import android.util.Log;

import com.fd.bookmanagement.BookManagementActivity;
import com.fd.bookmanagement.books.Book;

public class ISBNDBRequest extends BookRequest{

	/**
	 * For debugging purpose
	 */
	public static String DEBUGRESPONSESTRING = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
			"<ISBNdb server_time=\"2012-11-16T10:08:49Z\"> " +
			"<BookList total_results=\"1\" page_size=\"10\" page_number=\"1\" shown_results=\"1\">" +
			"<BookData book_id=\"okko_t01_le_cycle_de_leau\" isbn=\"2847891641\" isbn13=\"9782847891645\">" +
			"<Title>OKKO T01 : LE CYCLE DE L'EAU</Title>" +
			"<TitleLong></TitleLong>" +
			"<AuthorsText>HUB, coucou, </AuthorsText>" +
			"<PublisherText publisher_id=\"delcourt\">DELCOURT</PublisherText>" +
			"</BookData>" +
			"</BookList>" +
			"</ISBNdb>" ;

	/*
	 * Access key for http://isbndb.com
	 * To get an access, you should subscribe to the site and generate an access key
	 */
	public static String accessKey = null ;

	/**
	 * Information source
	 */
	private static final String sourceInformation = "http://isbndb.com";

	/**
	 * 
	 * @param listener listener called when the operation is done
	 * @throws Exception if no access key have been specified
	 */
	public ISBNDBRequest (OnRequestCompleted listener) throws Exception {

		super(listener);
		
		if (accessKey == null) { // no access key
			throw new Exception();
		}
	}

	/*
	 * Send a request based on ISBN code to http://isbndb.com.
	 * This web site needs an access key that should created by subscription.
	 * Other site should be integrated so an intermediate XML format should be provided in order to get same answer from different web sites
	 * @param isbnCode ISBN code
	 * @return XML file based on the answer from http://isbndb.com. The XML file may contain a TAG <Error value="Bad request status" status="..."> 
	 * @exception ClientProtocolException
	 * @exception IOException
	 * Example :
	 * <?xml version="1.0" encoding="UTF-8"?>
	 * <ISBNdb server_time="2012-11-16T10:08:49Z">
	 * <BookList total_results="1" page_size="10" page_number="1" shown_results="1">
	 * <BookData book_id="okko_t01_le_cycle_de_leau" isbn="2847891641" isbn13="9782847891645">
	 * <Title>OKKO T01 : LE CYCLE DE L'EAU</Title>
	 * <TitleLong></TitleLong>
	 * <AuthorsText>HUB, </AuthorsText>
	 * <PublisherText publisher_id="delcourt">DELCOURT</PublisherText>
	 * </BookData>
	 * </BookList>
	 * </ISBNdb>
	 * 
	 * In case of error:
	 * <?xml version="1.0" encoding="UTF-8"?>
	 * <Error value="incorrect status from " + <url> " request" servermessage="">
	 * </Error>
	 * <ErrorMessage>Access key error</ErrorMessage>
	 */
	public void sendIsbnRequest (String isbnCode) {

		String url = sourceInformation + "/api/books.xml?access_key=" + accessKey + "&index1=isbn&value1=" + isbnCode ;

		Log.d(BookManagementActivity.TAG_LOG, "url = " + url);

		this.asyncTask = new AsyncSendURL(listener) ;
		this.asyncTask.execute(url);

		this.isbnCode = isbnCode ;
	}

	/**
	 * This method parse string returned by parseIsbnDbCom to provide a common 
	 * @param value string containing the answer from ISBN DB
	 * @return string with few information about Title, publishers and authors
	 */
	@Override
	public Book parseIsbnRequest (String value) {

		Book bd = null ;

		if (this.asyncTask.getErrorCode() != BookRequest.REQUEST_COMPLETED) {

			errorCode = asyncTask.getErrorCode();
			
			String errorTag = AsyncSendURL.parseElement(value,"ERROR") ;
			if (errorTag != null) {
				Log.e(BookManagementActivity.TAG_LOG, "ISBNDB#parseIsbnDbCom: Error during the request : " + errorTag);
				error = errorTag ;
			}
			return null ;
		}
		else {

			errorCode = asyncTask.getErrorCode();
			error = null ;

			// Check if an error appears
			if (value.contains("<ErrorMessage>Access key error</ErrorMessage>")) {
				errorCode = BookRequest.ERRORCODE_ACCESSKEY ;
			}
			else {

				// Parsing of the message from ISBNDB
				String titre = AsyncSendURL.parseElement(value, "Title");
				if (titre != null && titre.length() > 0) {
					bd = new Book(titre);
					bd.setIsbn(isbnCode);
					bd.setSourceInformation(sourceInformation);
				}

				String authors = AsyncSendURL.parseElement(value,"AuthorsText") ;
				if (authors != null && authors.length() > 0) {

					if (bd == null) bd = new Book("Unknown");

					if (authors.charAt(authors.length()-2) == ',') {
						bd.setAuthors(authors.substring(0,authors.length()-2));
					}
					else {
						bd.setAuthors(authors);
					}
				}

				String publisher = AsyncSendURL.parseElement(value,"PublisherText") ;
				if (publisher != null && publisher.length() > 0) {

					if (bd == null) bd = new Book("Unknown");

					bd.setPublisher(publisher);
				}

				if (bd == null) {
					errorCode = BookRequest.ERRORCODE_UNKNOWNISBN ;
				}
			}
		}

		return bd ;
	}

	/**
	 * ISBN
	 * @return ISBN code
	 */
	public String getIsbn() {
		return this.isbnCode ;
	}

}
