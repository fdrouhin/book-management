package com.fd.bookmanagement.isbnrequests;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;

public class AsyncSendURL extends AsyncTask<String, Integer, String> {

	/**
	 * 
	 */
	private HttpClient httpclient = new DefaultHttpClient();

	/**
	 * Listener called when the operation is done
	 */
	private OnRequestCompleted listener;

	/**
	 * for debugging purpose
	 */
	public static final boolean DEBUG = false ;

	/**
	 * 
	 * @param Listener
	 */
	public AsyncSendURL(OnRequestCompleted listener) {
		this.listener = listener ;
	}
	
	/**
	 * Error code
	 */
	private int errorCode = BookRequest.REQUEST_COMPLETED ;

	/**
	 * 
	 */
	@Override
	protected String doInBackground(String... params) {

		String responseString = null ;
		String url = params[0];

		try {
			HttpResponse response;

			response = httpclient.execute(new HttpGet(url));

			StatusLine statusLine = response.getStatusLine();

			if(statusLine.getStatusCode() == HttpStatus.SC_OK){

				ByteArrayOutputStream out = new ByteArrayOutputStream();
				response.getEntity().writeTo(out);
				out.close();
				responseString = out.toString();
				
				errorCode =  BookRequest.REQUEST_COMPLETED ;
	
			} else {
				//Closes the connection.
				response.getEntity().getContent().close();

				responseString = "<ERROR>Incorrect status from " + url + " request status = " + statusLine.getStatusCode() + 
						", reason : " + statusLine.getReasonPhrase() + "</ERROR>" ;
				
				errorCode =  BookRequest.ERRORCODE_BADREQUESTSTATUS ; 

			}
		} catch (ClientProtocolException e) {
			responseString = "<ERROR>ClientProtocolException " + e + ": " + e.getMessage() + "</ERROR>";
			errorCode =  BookRequest.ERRORCODE_HTTPREQUEST ; 
		} catch (IOException e) {
			responseString = "<ERROR>IOException " + e + ": " + e.getMessage() + "</ERROR>";
			errorCode =  BookRequest.ERRORCODE_HTTPREQUEST ; 
		} catch (Exception e) {
			responseString = "<ERROR>Exception " + e + ": " + e.getMessage() + "</ERROR>";
			errorCode =  BookRequest.ERRORCODE_HTTPREQUEST ; 
		}

		return responseString ;
	}

	/**
	 * Call a listener when the operation is completed
	 * @param responseString answer of the request
	 */
	@Override
	protected void onPostExecute(String responseString) {

		if (listener != null) {
			//Log.d(BookManagementActivity.TAG_LOG,"AsyncSendURL#onPostExecute: " + responseString);
			listener.onIsbnRequestCompleted(responseString);
		}
	}
	
	/**
	 * @return error cod
	 */
	public int getErrorCode() {
		return errorCode;
	}

	/**
	 * Retrieve a tag in an XML data stating by <tag>...</tag>
	 * @param tag tag to be done
	 * 	"<PublisherText publisher_id=\"delcourt\">DELCOURT</PublisherText>"
	 */
	public static String parseElement(String xml, String tag) {

		String value = null ;

		if (xml.contains(tag)) {
			int debuti = xml.indexOf("<" + tag);

			if (debuti != -1) {
				String val = xml.substring(debuti);

				int debut = val.indexOf(">") + 1 ;
				int fin = val.indexOf("</" + tag + ">") ;

				if (debuti != -1 && debut !=-1 && fin != -1) {
					value = val.substring(debut,fin);
				}
			}
		}

		return value ;
	}

}
