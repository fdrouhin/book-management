package com.fd.bookmanagement.isbnrequests;

import com.fd.bookmanagement.books.Book;

public abstract class BookRequest {

	/**
	 * HTTP request error
	 */
	public static final int ERRORCODE_HTTPREQUEST = -1 ;
	
	/**
	 * Access key error
	 */
	public static final int ERRORCODE_ACCESSKEY = 1 ;
	
	/**
	 * ISBN not recognize
	 */
	public static final int ERRORCODE_UNKNOWNISBN = 2 ;

	/**
	 * Request completed
	 */
	public static final int REQUEST_COMPLETED = 0 ;

	/**
	 * Bad status when the request finished
	 */
	public static final int ERRORCODE_BADREQUESTSTATUS = -2;

	/**
	 * Error code
	 */
	protected int errorCode = BookRequest.REQUEST_COMPLETED ;

	/**
	 * Callback when the resource is completed
	 */
	protected OnRequestCompleted listener ;

	/**
	 * ISBN code
	 */
	protected String isbnCode ;

	/**
	 * Error message
	 */
	protected String error ;

	/**
	 * Async task
	 */
	protected AsyncSendURL asyncTask ;
	
	/**
	 * Add a listener when the request is completed
	 * @param listener
	 */
	public BookRequest(OnRequestCompleted listener) {
		this.listener = listener ;
	}
	
	/**
	 * @return the error get by the request
	 */
	public String getError() {
		return error ;
	}

	/**
	 * Error code during the parsing!!!
	 * @return
	 */
	public int getErrorCode() {
		return errorCode ;
	}
	
	/**
	 * ISBN
	 * @return ISBN code
	 */
	public String getIsbn() {
		return this.isbnCode ;
	}

	/**
	 * Send a cancel to the asynctask
	 * @param b cancel ?
	 */
	public void cancel(boolean b) {

		if (asyncTask != null) asyncTask.cancel(b);
	}

	/**
	 * Parse the answer already get by sendIsbnRequest
	 */
	public abstract Book parseIsbnRequest(String responseString) ;

	/*
	 * Send a request based on ISBN code to google book
	 */
	public abstract void sendIsbnRequest (String isbnCode) ;

}
