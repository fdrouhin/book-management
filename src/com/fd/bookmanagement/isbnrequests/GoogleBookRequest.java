package com.fd.bookmanagement.isbnrequests;

import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;

import com.fd.bookmanagement.BookManagementActivity;
import com.fd.bookmanagement.books.Book;

public class GoogleBookRequest extends BookRequest {

	/**
	 * Information source
	 */
	private static final String sourceInformation = "https://www.googleapis.com";

	/**
	 * 
	 * @param listener listener called when the operation is done
	 * @throws Exception if no access key have been specified
	 */
	public GoogleBookRequest (OnRequestCompleted listener)  {
		super(listener);
	}

	/*
	 * Send a request based on ISBN code to google book
	 */
	public void sendIsbnRequest (String isbnCode) {

		String url = sourceInformation + "/books/v1/volumes?q=isbn+" + isbnCode ;

		Log.d(BookManagementActivity.TAG_LOG, "url = " + url);

		this.asyncTask = new AsyncSendURL(listener) ;
		this.asyncTask.execute(url);

		this.isbnCode = isbnCode ;
	}

	/**
	 * This method parse string returned by parseIsbnDbCom to provide a common 
	 * @param value string containing the answer from ISBN DB
	 * @return string with few information about Title, publishers and authors
	 */
	@Override
	public Book parseIsbnRequest (String value) {

		Log.d(BookManagementActivity.TAG_LOG, value);
		
		Book bd = null ;

		if (this.asyncTask.getErrorCode() != BookRequest.REQUEST_COMPLETED) {

			errorCode = asyncTask.getErrorCode();

			String errorTag = AsyncSendURL.parseElement(value,"ERROR") ;
			if (errorTag != null) {
				Log.e(BookManagementActivity.TAG_LOG, "GoogleBookRequest#parseIsbnDbCom: Error during the request : " + errorTag);
				error = errorTag ;
			}
			return null ;
		}
		else {

			errorCode = asyncTask.getErrorCode();
			error = null ;

			try {
				JSONArray ja = new JSONArray(value);
				for (int i = 0; i < ja.length(); i++) {
					org.json.JSONObject jo = (org.json.JSONObject) ja.get(i);

					Log.d(BookManagementActivity.TAG_LOG, jo.toString());
				}
			} catch (JSONException e) {
				Log.e(BookManagementActivity.TAG_LOG, "GoogleBookRequest#parseIsbnDbCom: Error during the request (" + e + "): " + e.getMessage());
				errorCode = BookRequest.ERRORCODE_BADREQUESTSTATUS;
				error = e + ": " + e.getMessage();
			}

		}

		return bd ;
	}
}
