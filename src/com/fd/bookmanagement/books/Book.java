/**
 * 
 */
package com.fd.bookmanagement.books;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.fd.bookmanagement.BookManagementActivity;

/**
 * The book definition
 * @author Frederic.Drouhin
 * Parcelable is not used for the time being since I got error when I parcel the Bitmap. I pass by a static variable in the main activity
 *
 */
public class Book implements Parcelable, Comparable<Book> {

	/**
	 * Cover by default, should be set to be used in Parceable configuration
	 */
	public static Bitmap defaultCover ;

	/**
	 * Serie
	 */
	private String serie ;

	/**
	 * Volume
	 */
	private Integer volume ;

	/**
	 * Titre
	 */
	private String title ;

	/**
	 * Liste des auteurs
	 */
	private String authors ;

	/**
	 * Publisher
	 */
	private String publisher ;

	/**
	 * ISBN
	 */
	private String isbn ;

	/**
	 * private String source ;
	 */
	private String sourceInformation ;

	/**
	 * Image
	 */
	private Bitmap cover ;

	/**
	 * Gender
	 */
	private String gender ;

	/**
	 * Synopsis
	 */
	private String synopsis ;

	/**
	 * print date
	 */
	public Date printDate ;

	/**
	 * Buy date
	 */
	public Date buyDate ;

	/**
	 * Collection
	 */
	public String collection ;

	/**
	 * 
	 */
	public static String dateFormat = "dd/MM/yyyy" ;

	/**
	 * @param serie
	 * @param volume
	 * @param title
	 * @param authors
	 * @param publisher
	 * @param isbn
	 * @param sourceInformation
	 * @param cover
	 * @param gender
	 * @param synopsis
	 * @param printDate
	 * @param buyDate
	 * @param collection
	 */
	public Book(String serie, Integer volume, String title, String authors,
			String publisher, String isbn, String sourceInformation,
			Bitmap cover, String gender, String synopsis, Date printDate,
			Date buyDate, String collection) {

		super();
		this.serie = serie;
		this.volume = volume;
		this.title = title;
		this.authors = authors ;
		this.publisher = publisher;
		this.isbn = isbn;
		this.sourceInformation = sourceInformation;
		this.cover = cover;
		this.gender = gender;
		this.synopsis = synopsis;
		this.printDate = printDate;

		if (buyDate == null) {
			this.buyDate = new GregorianCalendar().getTime();
		}
		else {
			this.buyDate = buyDate;
		}

		this.collection = collection;
	}

	/**
	 * 
	 * @param title
	 */
	public Book(String title) {
		this(null,null,title,null,
				null,null,null,
				null,null,null,null,
				null,null);
	}



	/**
	 * @param serie serie
	 * @param volume volume
	 * @param title title
	 * @param authors author(s)
	 * @param publisher publisher
	 * @param isbn isbn code
	 * @param sourceInformation where the information was get
	 * @param cover cover (if has one)
	 * @param gender gender (if has one)
	 */
	public Book(String serie, int volume, String title, String authors,
			String publisher, String isbn, String sourceInformation,
			Bitmap cover, String gender) {
		this(serie,volume,title,authors,publisher,isbn,sourceInformation,cover,gender,null,null,null,null);
	}
	/**
	 * @param title title
	 * @param authors author(s)
	 * @param publisher publisher
	 * @param isbn isbn code
	 * @param sourceInformation where the information was get
	 * @param cover cover (if has one)
	 * @param gender gender (if has one)
	 */
	public Book(String title, String authors,
			String publisher, String isbn, String sourceInformation,
			Bitmap cover, String gender) {

		this(null,null,title,authors,publisher,isbn,sourceInformation,cover,gender,null,null,null,null);
	}

	/**
	 * @param title title
	 * @param authors author(s)
	 * @param publisher publisher
	 * @param isbn ISBN code
	 * @param sourceInformation where the information was get
	 * @param cover cover (if has one)
	 * @param gender gender (if has one)
	 */
	public Book(String title, String authors,
			String publisher, String isbn, String sourceInformation) {
		this(null,null,title,authors,publisher,isbn,sourceInformation,null,null,null,null,null,null);
	}


	/**
	 * 
	 * @return XML with all data coming from description
	 */
	public String toXML() {
		/* <BOOK>
		 * <Title>OKKO T01 : LE CYCLE DE L'EAU</Title>
		 * <Author>HUB</AuthorText>
		 * <Author>HUB1</AuthorText>
		 * <Publisher>DELCOURT</Publisher>
		 * <FROM>http://isbndb.com</FROM>
		 * <IBSN>2847891641</ISBN>
		 * </BOOK>*/

		//String serie, Integer volume, String title, String authors,
		// String publisher, String isbn, String sourceInformation,
		// Bitmap cover, String gender, String synopsis, Date printDate,
		// Date buyDate, String collection

		StringBuffer xml = new StringBuffer("<BOOK>").append("\n");
		xml.append("<Serie>").append(serie).append("</Serie>\n");		
		xml.append("<Volume>").append(volume).append("</Volume>\n");
		xml.append("<Title>").append(title).append("</Title>\n");
		xml.append("<Authors>").append(authors).append("</Authors>\n");
		xml.append("<Publisher>").append(publisher).append("</Publisher>\n");
		xml.append("<Isbn>").append(isbn).append("</Isbn>\n");
		xml.append("<SourceInformation>").append(isbn).append("</SourceInformation>\n");
		xml.append("<Gender>").append(gender).append("</Gender>\n");
		xml.append("<Synopsis>").append(synopsis).append("</Synopsis>\n");
		xml.append("<PrintDate>").append(dateToString(printDate)).append("</PrintDate>\n");
		xml.append("<BuyDate>").append(dateToString(buyDate)).append("</BuyDate>\n");
		xml.append("<Collection>").append(collection).append("</Collection>\n");
		xml.append("</BOOK>");

		return xml.toString();
	}

	/**
	 * Format used for GCS
	 * @param id start id
	 */
	public String toGCS(int id) {
		/*<item
			  id="3"
			  name="Okko #001 Le Cycle de l'eau I"
			  series="Okko"
			  volume="1"
			  title="Le Cycle de l'eau I"
			  writer="Humbert Chabuel - 'Hub'"
			  illustrator="Humbert Chabuel - 'Hub'"
			  colourist="Humbert Chabuel - 'Hub'"
			  publisher="Archaia Entertainment"
			  collection=""
			  publishdate="01/04/2010"
			  printdate="01/04/2010"
			  image=".BD_pictures/Okko__001_Le_Cycle_de_l_eau_I_0.jpg"
			  backpic=""
			  webPage="http://www.comicbookdb.com/issue.php?ID=198716##Comic Book DB"
			  added="12/11/2012"
			  isbn=""
			  type=""
			  category=""
			  format=""
			  numberboards="1"
			  signing="0"
			  cost=""
			  rating="0"
			  file=""
			  borrower="none"
			  lendDate=""
			  borrowings=""
			  favourite="0"
			  tags=""
			 >
			  <synopsis></synopsis>
			  <comment></comment>
			 </item>*/

		StringBuffer buff = new StringBuffer("<item").append("\n") ;

		buff.append("id=\"").append(id).append("\"").append("\n");

		buff.append("name=\"");
		if (serie != null) buff.append(serie);
		if (volume != null) buff.append(" #00").append(volume) ;
		if (title != null) buff.append(" ").append(title) ;
		buff.append("\"").append("\n");

		buff.append("volume=\"");
		if (volume != null) buff.append(volume);
		buff.append("\"").append("\n");

		buff.append("title=\"");
		if (title != null) buff.append(title);
		buff.append("\"").append("\n");

		buff.append("writer=\"");
		if (authors != null) buff.append(authors);
		buff.append("\"").append("\n");

		buff.append("illustrator=\"") ;
		//if (authors != null) buff.append(authors);
		buff.append("\"").append("\n");

		buff.append("colourist=\"");
		//if (authors != null) buff.append(authors);
		buff.append("\"").append("\n");

		buff.append("publisher=\"");
		if (publisher != null) buff.append(publisher);
		buff.append("\"").append("\n");

		buff.append("collection=\"");
		if (collection != null) buff.append(collection);
		buff.append("\"").append("\n");

		buff.append("publishdate=\"");
		if (printDate != null) buff.append(dateToString(printDate));
		buff.append("\"").append("\n");

		buff.append("printdate=\"");
		if (printDate != null) buff.append(dateToString(printDate));
		buff.append("\"").append("\n");

		buff.append("image=\"");
		//if (cover != null) buff.append("????");
		buff.append("\"").append("\n");

		buff.append("backpic=\"\"").append("\n"); ;

		buff.append("webPage=\"");
		if (sourceInformation != null) buff.append(sourceInformation);
		buff.append("\"").append("\n");

		buff.append("added=\"");
		if (buyDate != null) buff.append(dateToString(buyDate));
		buff.append("\"").append("\n");

		buff.append("isbn=\"");
		if (isbn != null) buff.append(isbn);
		buff.append("\"").append("\n");

		buff.append("type=\"");
		if (gender != null) buff.append(gender);
		buff.append("\"").append("\n");

		buff.append("type=\"");
		if (gender != null) buff.append(gender);
		buff.append("\"").append("\n");

		buff.append("category=\"");
		if (gender != null) buff.append(gender);
		buff.append("\"").append("\n");

		buff.append("format=\"\"").append("\n") ;

		buff.append("numberboards=\"\"").append("\n") ;

		buff.append("signing=\"\"").append("\n") ;

		buff.append("cost=\"\"").append("\n") ;

		buff.append("rating=\"\"").append("\n") ;

		buff.append("file=\"\"").append("\n") ;

		buff.append("borrower=\"none\"").append("\n") ;

		buff.append("lendDate=\"none\"").append("\n") ;

		buff.append("borrowings=\"\"").append("\n") ;

		buff.append("favourite=\"0\"").append("\n") ;

		buff.append("tags=\"\"").append("\n") ;

		buff.append(">").append("\n");

		buff.append("<synopsis>").append(synopsis).append("</synopsis>").append("\n");

		buff.append("<comment>").append(synopsis).append("</comment>").append("\n");

		buff.append("</item>").append("\n");

		return buff.toString();
	}



	/**
	 * @return the serie
	 */
	public String getSerie() {
		return serie;
	}



	/**
	 * @param serie the serie to set
	 */
	public void setSerie(String serie) {
		this.serie = serie;
	}



	/**
	 * @return the volume
	 */
	public Integer getVolume() {
		return volume;
	}



	/**
	 * @param volume the volume to set
	 */
	public void setVolume(Integer volume) {
		this.volume = volume;
	}

	/**
	 * @param volume the volume to set
	 */
	public void setVolume(String volume) {
		this.volume = Integer.parseInt(volume);
	}


	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}



	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}



	/**
	 * @return the authors
	 */
	public String getAuthors() {
		return authors;
	}



	/**
	 * @param authors the authors to set
	 */
	public void setAuthors(String authors) {
		this.authors = authors;
	}

	/**
	 * @return the publisher
	 */
	public String getPublisher() {
		return publisher;
	}



	/**
	 * @param publisher the publisher to set
	 */
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}



	/**
	 * @return the isbn
	 */
	public String getIsbn() {
		return isbn;
	}



	/**
	 * @param isbn the isbn to set
	 */
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}



	/**
	 * @return the sourceInformation
	 */
	public String getSourceInformation() {
		return sourceInformation;
	}



	/**
	 * @param sourceInformation the sourceInformation to set
	 */
	public void setSourceInformation(String sourceInformation) {
		this.sourceInformation = sourceInformation;
	}



	/**
	 * @return the cover
	 */
	public Bitmap getCover() {
		return cover;
	}



	/**
	 * @param cover the cover to set
	 */
	public void setCover(Bitmap cover) {
		this.cover = cover;
	}



	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}



	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return Title and author(s)
	 */
	public String getTitleAuthor() {

		int i = 0 ;

		StringBuffer buffer = new StringBuffer() ;
		if (serie != null) { buffer.append(serie) ; i = 1 ; }
		if (volume != null) {
			if (i == 1) { buffer.append(", ") ; i = 0 ; }
			buffer.append(volume) ; 
			i = 1 ; 
		}
		if (title != null) {
			if (i == 1) { buffer.append(", ") ; i = 0 ; }
			buffer.append(title) ; 
			i = 1 ; 
		}
		if (authors != null) {
			if (i == 1) { buffer.append(", ") ; i = 0 ; }
			buffer.append(authors) ; 
			i = 1 ;
		}

		return buffer.toString() ;
	}


	/**
	 * Return a String with title, author1, author2, ... depending on the number of characters specified
	 * @param nbChar number of character for a correct display
	 * @return string with title and authors
	 * @deprecated
	 */
	public String getTitleAuthor(int nbChar) {

		nbChar = nbChar - 4 ;

		StringBuffer buffer = new StringBuffer(addNbChars(serie, nbChar, ", ")) ;
		buffer.append(addNbChars(volume, nbChar-buffer.length(), ", ")) ;
		buffer.append(addNbChars(title, nbChar-buffer.length(), ", ")) ;

		if (authors != null) {
			buffer.append(addNbChars(authors, nbChar-buffer.length(), "")) ;
		}

		return buffer.toString() ;
	}

	/**
	 * Return a string with max nbChar
	 * @param line text to be truncated
	 * @param nbChar number of characters
	 * ]@param comma separator
	 * @return string with number of chars specified
	 * @deprecated
	 */
	private String addNbChars(Object line, int nbChar, String comma) {

		String value = "" ;

		if (nbChar > 0) {
			if (line != null) {
				if (line.toString().length() < (nbChar + 3)) {
					value = line + comma ;
				}
				else {
					value = line.toString().substring(0,nbChar-4) + " ...";
				}
			}
		}
		return value ;
	}

	/**
	 * @return synopsis synopsis
	 */
	public String getSynopsis() {
		return synopsis;
	}

	/**
	 * change the synopsis
	 * @param synopsis synopsis
	 */
	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}

	/**
	 * @return the printDate
	 */
	public Date getPrintDate() {
		return printDate;
	}

	/**
	 * @param printDate the printDate to set
	 */
	public void setPrintDate(Date printDate) {
		this.printDate = printDate;
	}

	/**
	 * @param buyDate the buyDate to set
	 */
	public void setPrintDate(String printDate) {

		this.printDate = stringToDate(printDate,dateFormat);
	}

	/**
	 * @param buyDate the buyDate to set
	 */
	public void setPrintDate(String printDate, String dateFormat) {

		this.printDate = stringToDate(printDate,dateFormat);
	}

	/**
	 * @return the buyDate
	 */
	public Date getBuyDate() {
		return buyDate;
	}

	/**
	 * @param buyDate the buyDate to set
	 */
	public void setBuyDate(Date buyDate) {
		this.buyDate = buyDate;
	}

	/**
	 * @param buyDate the buyDate to set
	 */
	public void setBuyDate(String buyDate) {
		this.buyDate = stringToDate(buyDate, dateFormat);
	}

	/**
	 * @param buyDate the buyDate to set
	 */
	public void setBuyDate(String buyDate, String dateFormat) {

		this.buyDate = stringToDate(buyDate,dateFormat);
	}

	/**
	 * Convert a String to a Date
	 * @param date date to convert
	 * @param dateFormat format
	 * @return Date converted in Date object
	 */
	public static Date stringToDate(String date, String dateFormat) {
		Date d = null ;

		try {
			d = new SimpleDateFormat(dateFormat).parse(date);
		}
		catch (Exception e) {
			Log.e(BookManagementActivity.TAG_LOG,"Book#stringToDate: exception in date conversion" + e + ": " + e.getMessage() + " for date = " + date + " and format = " + dateFormat);
		}

		return d;
	}

	/**
	 * Convert a Date to String
	 * @param date date
	 * @param dateFormat date format
	 * @return String date
	 */
	public static String dateToString(Date date) {

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Book.dateFormat);

		return (simpleDateFormat.format(date));
	}

	/**
	 * @return the collection
	 */
	public String getCollection() {
		return collection;
	}

	/**
	 * @param collection the collection to set
	 */
	public void setCollection(String collection) {
		this.collection = collection;
	}

	/**
	 * A book is comparable with another based on the ISBN and if no ISBN provided then on title and the first author compare to other authors from another book.
	 * @param another book
	 */
	@Override
	public int compareTo(Book another) {

		if (serie != null && another.serie != null) {
			int val = serie.compareTo(another.serie) ;
			if (val != 0) return val ;
		}

		if (volume != null && another.volume != null) {
			if (volume > another.volume) return 1 ;
			else if (volume < another.volume) return -1 ;
		}

		if (title != null && another.title != null) {
			int val = title.compareTo(another.title) ;
			if (val != 0) return val ;
		}

		if (authors != null && another.authors != null) {
			return authors.compareTo(another.authors);
		}

		return 0 ;
	}

	/*
		if (this.isbn != null && another.isbn != null) {
			return this.isbn.compareTo(another.isbn);
		}
		else {
			if (this.title != null && another.title != null) {
				if (this.title.equalsIgnoreCase(another.title)) {
					if (authors != null && another.authors != null) {
						return authors.compareTo(another.authors);
					}
				}
				else {
					return this.title.compareTo(another.title);
				}
			}
		}
	 */

	/* --------------------------------------------------------- */
	/* ------------------ Parcelable --------------------------- */
	/* --------------------------------------------------------- */

	/**
	 * 
	 */
	public void writeToParcelCoucou(Parcel dest, int flags) {
		//String serie, Integer volume, String title, String authors,
		// String publisher, String isbn, String sourceInformation,
		// Bitmap cover, String gender, String synopsis, Date printDate,
		// Date buyDate, String collection
		if (serie != null) dest.writeString(serie);
		else dest.writeString("");
		if (volume != null) dest.writeInt(volume);
		else dest.writeString("");
		if (authors != null) dest.writeString(authors);
		else dest.writeString("");
		if (publisher != null) dest.writeString(publisher);
		else dest.writeString("");
		if (isbn != null) dest.writeString(isbn);
		else dest.writeString("");
		if (sourceInformation != null) dest.writeString(sourceInformation);
		else dest.writeString("");
		if (cover != null) {
			dest.writeValue(cover);
		}
		//else dest.writeValue("");
		if (gender != null) dest.writeString(gender);
		else dest.writeString("");
		if (synopsis != null) dest.writeString(synopsis);
		if (printDate != null) dest.writeString(dateToString(printDate));
		else dest.writeString("");
		if (buyDate != null) dest.writeString(dateToString(buyDate));
		else dest.writeString("");
		if (buyDate != null) dest.writeString(collection);
		else dest.writeString("");
	}

	/**
	 * Create a book based on Parcelable data
	 * @param source parcel source
	 */
	public Book(Parcel source) {
		serie = source.readString();
		//Log.d(BookManagementActivity.TAG_LOG, "Book#Book(Parcel): source : " + "source : " + serie);
		if (serie != null && serie.length() == 0) serie = null ;

		volume = source.readInt();
		//Log.d(BookManagementActivity.TAG_LOG, "Book#Book(Parcel): source : " + volume.toString());
		if (volume != null && volume == 0) volume = null ;

		title = source.readString();
		//Log.d(BookManagementActivity.TAG_LOG, "Book#Book(Parcel): source : " + title);
		if (title != null && title.length() == 0) title = null ;

		authors = source.readString();
		//Log.d(BookManagementActivity.TAG_LOG, "Book#Book(Parcel): source : " + authors);
		if (authors != null && authors.length() == 0) authors = null ;

		publisher = source.readString();
		//Log.d(BookManagementActivity.TAG_LOG, "Book#Book(Parcel): source : " + publisher);
		if (publisher != null && publisher.length() == 0) publisher = null ;

		isbn = source.readString();
		//Log.d(BookManagementActivity.TAG_LOG, "Book#Book(Parcel): source : " + isbn);
		if (isbn != null && isbn.length() == 0) isbn = null ;

		sourceInformation = source.readString();
		//Log.d(BookManagementActivity.TAG_LOG, "Book#Book(Parcel): source : " + sourceInformation);
		if (sourceInformation != null && sourceInformation.length() == 0) sourceInformation = null ;

		Integer val = source.readInt() ;
		//Log.d(BookManagementActivity.TAG_LOG, "Book#Book(Parcel): isCover ? : " + val);

		if (val != 0) {
			this.cover = Bitmap.CREATOR.createFromParcel(source) ;
		}
		else {
			this.cover = null ;
		}

		gender = source.readString();
		//Log.d(BookManagementActivity.TAG_LOG, "Book#Book(Parcel): source : " + gender);
		if (gender != null && gender.length() == 0) gender = null ;

		synopsis = source.readString();
		//Log.d(BookManagementActivity.TAG_LOG, "Book#Book(Parcel): source : " + synopsis);		
		if (synopsis != null && synopsis.length() == 0) synopsis = null ;

		String str = source.readString();
		//Log.d(BookManagementActivity.TAG_LOG, "Book#Book(Parcel): source : " + str);
		if (str != null && str.length() == 0) printDate = null ;
		else printDate = stringToDate(str,dateFormat);

		str = source.readString();
		//Log.d(BookManagementActivity.TAG_LOG, "Book#Book(Parcel): source : " + str);
		if (str != null && str.length() == 0) buyDate = null ;
		else buyDate = stringToDate(str,dateFormat);

		collection = source.readString();
		//Log.d(BookManagementActivity.TAG_LOG, "Book#Book(Parcel): source : " + collection);
		if (collection != null && collection.length() == 0) collection = null ;
	}


	/**
	 * 
	 */
	public void writeToParcel(Parcel dest, int flags) {
		//String serie, Integer volume, String title, String authors,
		// String publisher, String isbn, String sourceInformation,
		// Bitmap cover, String gender, String synopsis, Date printDate,
		// Date buyDate, String collection
		dest.writeString(serie);
		dest.writeInt(volume);
		dest.writeString(authors);
		dest.writeString(publisher);
		dest.writeString(isbn);
		dest.writeString(sourceInformation);

		/*Bitmap bitmap ;
		if (cover != null) bitmap = cover ;
		else if (defaultCover != null) bitmap = defaultCover ;
		else {
			bitmap = Bitmap.createBitmap(128, 128, Bitmap.Config.ARGB_8888);
		}		
		ByteArrayOutputStream bs = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 50, bs);
		dest.writeByteArray(bs.toByteArray());
		//bitmap.writeToParcel(dest, PARCELABLE_WRITE_RETURN_VALUE);*/

		dest.writeInt(Integer.valueOf(0));

		dest.writeString(gender);
		dest.writeString(synopsis);
		if (printDate != null) dest.writeString(dateToString(printDate));
		else dest.writeString(null);
		if (buyDate != null) dest.writeString(dateToString(buyDate));
		else dest.writeString(null);
		dest.writeString(collection);
	}


	/**
	 * To create a book from Parcelable
	 */
	public static final Parcelable.Creator<Book> CREATOR = new Parcelable.Creator<Book>() {
		public Book createFromParcel(Parcel source) {
			return new Book(source) ;
		}

		@Override
		public Book[] newArray(int size) {
			return new Book[size];
		}
	};

	/**
	 * 
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/**
	 * Easier solution (from my point of view) to get all attributes for the book adapter). The bitmap is ignored in that case 
	 * @param position
	 * @return
	 */
	public Object getAttribute(int position) {
		//String serie, Integer volume, String title, String authors,
		// String publisher, String isbn, String sourceInformation,
		// Bitmap cover, String gender, String synopsis, Date printDate,
		// Date buyDate, String collection

		switch (position) {
		case 0:
			return title ;
		case 1:
			return serie ; 
		case 2:
			return volume ;
		case 3:
			return authors ;
		case 4:
			return publisher ;
		case 5:
			return isbn ;
		case 6:
			return sourceInformation ;
		case 7: 
			return gender ;
		case 8:
			return synopsis ;
		case 9:
			if (printDate != null) 
				return dateToString(printDate);
			else return null ;
		case 10:
			if (buyDate != null) 
				return dateToString(buyDate);
			else return null ;
		case 11:
			return collection ;
		default:
			return null ;

		}
	}

	/**
	 * Easier solution (from my point of view) to get all attributes for the book adapter). The bitmap is ignored in that case 
	 * @param position
	 * @return
	 */
	public static int getAttributeName(int position) {
		//String serie, Integer volume, String title, String authors,
		// String publisher, String isbn, String sourceInformation,
		// Bitmap cover, String gender, String synopsis, Date printDate,
		// Date buyDate, String collection

		switch (position) {
		case 0:
			return com.fd.bookmanagement.R.string.title ;
		case 1:
			return com.fd.bookmanagement.R.string.serie ;
		case 2:
			return com.fd.bookmanagement.R.string.volume ;
		case 3:
			return com.fd.bookmanagement.R.string.authors ;
		case 4:
			return com.fd.bookmanagement.R.string.publisher ;
		case 5:
			return com.fd.bookmanagement.R.string.isbn ;
		case 6:
			return com.fd.bookmanagement.R.string.sourceInformation ;
		case 7: 
			return com.fd.bookmanagement.R.string.gender ;
		case 8:
			return com.fd.bookmanagement.R.string.synopsis ;
		case 9:
			return com.fd.bookmanagement.R.string.printDate ;
		case 10:
			return com.fd.bookmanagement.R.string.buyDate ;
		case 11:
			return com.fd.bookmanagement.R.string.collection ;
		default:
			return -1 ;
		}
	}

	/**
	 * Number of attritutes, take care about the method getAttribute and getAttributeName
	 * @return
	 */
	public static int getNumberAttributes() {

		return 12 ;
	}

	/**
	 * Write a library to a file name
	 * @param fileName file name
	 * @param library list of books
	 * @param id id for GCS
	 * @throws IOException in case of problem in writing the file
	 */
	public static void libraryToGCSXML(String fileName, List<Book> library, int id) throws IOException {

		FileWriter w = null;
		BufferedWriter bw = null ;
		try {
			w = new FileWriter(fileName) ;
			bw = new BufferedWriter ( w ) ; 

			for (Book a: library) {

				bw.write(a.toGCS(id++));
			}
		}
		finally {
			bw.close();
			w.close();
		}

	}

	/**
	 * Write a library to a file name
	 * @param fileName file name
	 * @param library list of books
	 * @throws IOException in case of problem in writing the file
	 * @see Book#libraryToGCSXML(String, List, int)
	 */
	public static void libraryToGCSXML(String fileName, List<Book> library) throws IOException {

		libraryToGCSXML(fileName, library, 1);
	}

}
