package com.fd.bookmanagement;

import android.content.SearchRecentSuggestionsProvider;

/**
 * For recent search and title provider
 * @author Frederic.Drouhin
 *
 */
public class TitleAuthorSuggestionProvider extends SearchRecentSuggestionsProvider {
	
	public final static String AUTHORITY = "com.fd.bookmanagement.TitleAuthorSuggestionProvider";
    public final static int MODE = DATABASE_MODE_QUERIES;

    /**
     * 
     */
    public TitleAuthorSuggestionProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }

}
