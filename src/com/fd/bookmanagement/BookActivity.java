package com.fd.bookmanagement;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.fd.bookmanagement.books.Book;
import com.fd.bookmanagement.isbnrequests.BookRequest;
import com.fd.bookmanagement.isbnrequests.GoogleBookRequest;
import com.fd.bookmanagement.isbnrequests.ISBNDBRequest;
import com.fd.bookmanagement.isbnrequests.OnRequestCompleted;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class BookActivity extends ListActivity implements OnRequestCompleted {

	/**
	 * Activity request code : new book
	 */
	public static final int ACTIVITY_REQUESTCODE_NEWBOOK = 0 ;

	/**
	 * Activity request code : book scan
	 */
	public static final int ACTIVITY_REQUESTCODE_BOOKSCAN = 1 ;

	/**
	 * Activity request code : book scan
	 */
	public static final int ACTIVITY_REQUESTCODE_BOOKDISPLAY = 2 ;

	/**
	 * Camera request
	 */
	public static final int CAMERA_REQUEST = 3 ;

	/**
	 * For BookActivity - extra with serialize/parcelable data => not working
	 */
	// public static final String TAGBOOKEXTRA = "TAG_BOOK_EXTRA";

	/**
	 * Operation to be done in BookActivity
	 */
	public static final String TAG_REQUEST_MODE = "REQUEST_MODE";

	/**
	 * Used for book modification
	 */
	public static final String POSITION = "POSITION";

	/**
	 * Which request
	 */
	private int requestCode = -1 ;

	/**
	 * Book Adapter
	 */
	private BookAdapter bookAdapter ;

	/**
	 * The book managed by the activity
	 */
	private Book book ;

	/**
	 * ISBN DB API
	 */
	public static BookRequest bookRequest[] = new BookRequest[1] ;

	/**
	 * Google API book
	 */
	//public static GoogleBookRequest bookRequest = null ;

	/**
	 * ISBN for debugging only when camera is emulated
	 */
	public static String ISBNEXAMPLE ; 

	/**
	 * Components displayed: ProgressBar
	 */
	public ProgressBar progressBarScan;

	/**
	 * Components displayed: titreView
	 */
	public TextView titreView;

	/**
	 * Components displayed: Cover
	 */
	public ImageButton cover ;

	/* ------------------------------------------------------------------------------------ */
	/* ------------------------------------------------------------------------------------ */
	/* ------------------------------------------------------------------------------------ */

	/**
	 * onCreate
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.book_layout);

		// Get the views
		if (titreView == null) {
			titreView = (TextView)findViewById(R.id.iBookTitle);
			cover = (ImageButton)findViewById(R.id.iBookImage);
			progressBarScan = (ProgressBar) findViewById(R.id.progressBarSearch);
		}

		// Create a motor of search (only once)
		try {
			if (bookRequest[0] == null) bookRequest[0] = new ISBNDBRequest(this);

			if (bookRequest.length > 1) bookRequest[1] = new GoogleBookRequest(this);
		} catch (Exception e) {
			Utils.alertDialog(this.getResources().getString(R.string.errorNoAccessKeyMessage), Utils.ERROR, Utils.OK);
			finish();
		}

		// Extra with book
		//Book book = (Book) getIntent().getParcelableExtra(BookManagementActivity.TAGBOOKEXTRA);
		// get the book and use it
		book = BookManagementActivity.activityBook ;

		requestCode = getIntent().getIntExtra(BookActivity.TAG_REQUEST_MODE, -1);
		if (requestCode == -1) {
			Log.e(BookManagementActivity.TAG_LOG,"Error in BookActivity: mode request is unknown !!!");
			finish();
		}

		// Scan, new book or display book
		if (book == null) {

			book = new Book("");

			// put data
			if (requestCode == ACTIVITY_REQUESTCODE_BOOKSCAN) {
				cover.setImageBitmap(BitmapFactory.decodeResource(this.getResources(), R.drawable.barcode_scanner_128));
				titreView.setText(this.getResources().getString(R.string.scanMessage));
			}
			else { // requestCode => ACTIVITY_REQUESTCODE_NEWBOOK
				titreView.setText(this.getResources().getString(R.string.handEnterInformation));
			}
		}
		else { // requestCode == ACTIVITY_REQUESTCODE_BOOKDISPLAY
			cover.setClickable(false);
			// put data
			if (book.getCover() != null) cover.setImageBitmap(book.getCover());
			titreView.setText(book.getTitle());
		}

		// The book adapter, ListView
		bookAdapter = new BookAdapter(this,book);
		setListAdapter(bookAdapter);

		// Book not used
		book = null ;

		// Context menu
		registerForContextMenu(this.getListView());
	}

	/**
	 * Options menu - create a search widget following the requirements provided by http://developer.android.com/guide/topics/search/search-dialog.html#UsingSearchWidget
	 * @param menu options menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the options menu from XML
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.bookmodification_menu, menu);

		return true ;
	}

	/* ------------------------------------------------------------------------------------------- */
	/* ------------------------------ Methods for scanning an ISBN code -------------------------- */
	/* ------------------------------------------------------------------------------------------- */


	/** 
	 * Scan the ISBN code with the help of Zxing software
	 * see http://code.google.com/p/zxing/wiki/ScanningViaIntent
	 * @param v view clicked
	 */
	public void bScanIsbn(View v) {
		IntentIntegrator integrator = new IntentIntegrator(this);
		integrator.initiateScan();
	}

	/**
	 * Result of the activities
	 * http://code.google.com/p/zxing/wiki/ScanningViaIntent
	 */
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

		// Scan for ISBN code
		if (requestCode == IntentIntegrator.REQUEST_CODE) {

			if (ISBNEXAMPLE != null) {
				bookAdapter.book.setIsbn(ISBNEXAMPLE) ;
				bookAdapter.notifyDataSetChanged();

				manageIsbn(ISBNEXAMPLE);
			}
			else {

				IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);

				if (result != null) { // do I get the result code => does not matter but should be investigated

					String contents = null ;
					if (result != null) contents = result.getContents();

					if (contents != null) {
						//Log.d(BookManagementActivity.TAG_LOG, "scan : Result succeed = " + contents);
						//Toast.makeText(this, contents, Toast.LENGTH_LONG).show();
						bookAdapter.book.setIsbn(contents);
						bookAdapter.notifyDataSetChanged();

						manageIsbn(contents);
					} /*else {
					Utils.alertDialog(this.getResources().getString(R.string.errorScanMessage));
				}*/
				}
			}
		}
		// Modification of book
		else if (requestCode == BookActivity.ACTIVITY_REQUESTCODE_BOOKDISPLAY) {

			if (resultCode == RESULT_OK) {
				if (BookManagementActivity.activityBook != null) {
					if (BookManagementActivity.activityBook.getTitle() != null) {
						this.titreView.setText(BookManagementActivity.activityBook.getTitle());
					}
					bookAdapter.setBook(BookManagementActivity.activityBook) ;
					bookAdapter.notifyDataSetChanged();
				}
			}

		}
		// Took a photo
		else if (requestCode == BookActivity.CAMERA_REQUEST) {

			Toast.makeText(this, "End of Photo, result" + resultCode + " / " + RESULT_OK + " / " + RESULT_CANCELED, Toast.LENGTH_LONG).show();

			if (resultCode == RESULT_OK) {
				if (book != null) {
					Bitmap photo = (Bitmap) intent.getExtras().get("data"); 
					bookAdapter.book.setCover(photo);
					cover.setImageBitmap(photo);

					Toast.makeText(this, "Got a photo !!!!", Toast.LENGTH_LONG).show();
				}
			}
			else if (BookManagementActivity.debug) {
				if (book != null) {
					Bitmap photo = BitmapFactory.decodeResource(this.getResources(), R.drawable.okkofeu1);
					bookAdapter.book.setCover(photo);
					cover.setImageBitmap(photo);

					Toast.makeText(this, "Got a photo !!!! in debug mode !", Toast.LENGTH_LONG).show();
				}
			}
		}
	}	

	/**
	 * Manage ISBN code
	 * @param isbn ISBN code
	 */
	private void manageIsbn(String isbn) {

		bookAdapter.book.setIsbn(isbn);
		bookAdapter.notifyDataSetChanged();

		titreView.setText("Search on web for ISBN: " + bookAdapter.book.getIsbn()) ;

		// send a request to API to get information about the book
		this.book = null ;

		Log.d(BookManagementActivity.TAG_LOG,"Book request[0] = " + bookRequest[0]);

		bookRequest[0].sendIsbnRequest(isbn) ;

		// Just a display for the user to see that the application is searching
		progressBarScan.setVisibility(View.VISIBLE);
		cover.setVisibility(View.GONE);
	}

	/**
	 * When request is finished, get the information and display it
	 */
	@Override
	public void onIsbnRequestCompleted(String responseString) {

		Book book = bookRequest[0].parseIsbnRequest(responseString);

		if (book == null) {

			switch (bookRequest[0].getErrorCode()) {
			case BookRequest.ERRORCODE_HTTPREQUEST:
				//Utils.alertDialog(this.getResources().getString(R.string.errorScanMessage),Utils.ERROR,Utils.OK);
				titreView.setText(this.getResources().getString(R.string.errorScanMessage));
				Log.i(BookManagementActivity.TAG_LOG,bookRequest[0].getError());
				break ;
			case BookRequest.ERRORCODE_ACCESSKEY:
				Utils.alertDialog(this.getResources().getString(R.string.errorBadAccessKeyMessage), Utils.ERROR, Utils.OK) ;
				titreView.setText(this.getResources().getString(R.string.errorBadAccessKeyMessage)) ;
				break ;
			case BookRequest.ERRORCODE_UNKNOWNISBN:
			default:
				titreView.setText(this.getResources().getString(R.string.errorIsbnMessage));
				break ;
			}

			// Book created
			this.book = null ;
		}
		else {
			// set the title
			titreView.setText(book.getTitle());

			// The book adapter, ListView
			bookAdapter.setBook(book); 
			// = new BookAdapter(this,book,this);
			//setListAdapter(bookAdapter);
			bookAdapter.notifyDataSetChanged();

			// Book created
			this.book = book ;
			BookManagementActivity.activityBook = book ;
		}


		// End of search
		progressBarScan.setVisibility(View.GONE);
		cover.setVisibility(View.VISIBLE);

		//Toast.makeText(this, "At the end of request : Book = " + this.book, Toast.LENGTH_LONG).show() ;
	}

	/* ------------------------------------------------------------------------------------------- */
	/* -------------------------------------- When user press back ------------------------------- */
	/* ------------------------------------------------------------------------------------------- */


	/* (non-Javadoc)
	 * @see android.app.Activity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		//Toast.makeText(this, "action onBackPressed", Toast.LENGTH_LONG).show();

		if (requestCode == ACTIVITY_REQUESTCODE_BOOKSCAN || requestCode == ACTIVITY_REQUESTCODE_NEWBOOK) {
			// Cancel the request
			bookRequest[0].cancel(true);

			// Check if a book operation is done
			//Intent a = new Intent();
			if (book != null) {
				Utils.alertDialog(this.getResources().getString(R.string.keepBookQuestion), Utils.QUESTION, Utils.YESNO);
				if (Utils.getPressButton() == Utils.YES) {
					setResult(RESULT_OK);
					BookManagementActivity.activityBook = book ;
				}
				else {
					setResult(RESULT_CANCELED);
					BookManagementActivity.activityBook = null ;				
				}
			}
			else {
				setResult(RESULT_CANCELED);
				BookManagementActivity.activityBook = null ;
			}
		}
		// Change the book if modified
		else if (requestCode == ACTIVITY_REQUESTCODE_BOOKDISPLAY) {

			// Intent for position
			Intent i = new Intent() ;
			i.putExtra(BookActivity.POSITION, getIntent().getIntExtra(BookActivity.POSITION, -1));
			// Modification operated and the book is modified in BookManagementActivity
			setResult(RESULT_OK, i);
		}

		// End of operation
		super.onBackPressed();
	}

	/* ------------------------------------------------------------------------------------------- */
	/* ---------------------- Context Menu ------------------------------------------------------- */
	/* ------------------------------------------------------------------------------------------- */

	/**
	 * 
	 */
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.bookcontext_menu, menu);
	}


	@Override  
	public boolean onContextItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.iModify:
			Intent intent = new Intent(this,BookModificationActivity.class);
			//intent.putExtra(TAGBOOKEXTRA, book);
			startActivityForResult(intent, BookActivity.ACTIVITY_REQUESTCODE_BOOKDISPLAY);
			break ;
		case R.id.iPhoto:
			Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE); 
			startActivityForResult(cameraIntent, BookActivity.CAMERA_REQUEST); 
			break ;
		case R.id.iSynopsis:
			Dialog dial = onCreateDialog(this.bookAdapter.book.getTitle(), this.bookAdapter.book.getSynopsis()) ;
			dial.show();
			break;
		}

		return true;  
	}

	/**
	 *  Item management from options menu
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		//Toast.makeText(this, "When ok is done : Book = " + this.book, Toast.LENGTH_LONG).show() ;

		switch (item.getItemId()) {
		case R.id.modify_ok:
			if (requestCode == ACTIVITY_REQUESTCODE_BOOKSCAN || requestCode == ACTIVITY_REQUESTCODE_NEWBOOK) {
				// Cancel the request
				bookRequest[0].cancel(true);

				// Check if a book operation is done
				//Intent a = new Intent();
				if (book != null) {

					//Toast.makeText(this, "Book ok and the result is ok", Toast.LENGTH_LONG).show();

					setResult(RESULT_OK);
					BookManagementActivity.activityBook = book ;
				}
				else {

					//Toast.makeText(this, "Book not ok and the result is not ok", Toast.LENGTH_LONG).show();

					setResult(RESULT_CANCELED);
					BookManagementActivity.activityBook = null ;
				}
			}
			finish();
			return true ;
		default:
			return false;
		}
	}

	/* ------------------------------------------------------------------------------------------- */
	/* ---------------------- List Item click ---------------------------------------------------- */
	/* ------------------------------------------------------------------------------------------- */

	/* (non-Javadoc)
	 * @see android.app.ListActivity#onListItemClick(android.widget.ListView, android.view.View, int, long)
	 */
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {

		Dialog dial = onCreateDialog(this.bookAdapter.book.getTitle(), this.bookAdapter.book.getSynopsis()) ;
		dial.show();
	}

	/**
	 * Create a dialog
	 * @param title book title
	 * @param synopsis book synopsis
	 * @return Dialog box
	 */
	public Dialog onCreateDialog(String title, String synopsis) {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		// Get the layout inflater
		LayoutInflater inflater = this.getLayoutInflater();

		// Inflate and set the layout for the dialog
		// Pass null as the parent view because its going in the dialog layout
		builder.setView(inflater.inflate(R.layout.dialog_titlesynopsis, null));
		/*
	    // Add action buttons
	           .setPositiveButton(R.string.dialog_titlesynopsis, new DialogInterface.OnClickListener() {
	               @Override
	               public void onClick(DialogInterface dialog, int id) {
	                   // sign in the user ...
	               }
	           })
	           .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id) {
	                   LoginDialogFragment.this.getDialog().cancel();
	               }
	           });      
		 */

		builder.setMessage(R.id.contextMenuTitle).setTitle(title);
		builder.setMessage(R.id.contextMenuSynopsis).setMessage(synopsis);

		return builder.create();
	}

}