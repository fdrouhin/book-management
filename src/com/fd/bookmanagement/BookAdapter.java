package com.fd.bookmanagement;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fd.bookmanagement.books.Book;

public class BookAdapter extends BaseAdapter {

	/**
	 * Max char in a TextView
	 */
	//private static final int maxCharacterInLine = 100; // not anymore needed since the max number of lines is specified in the TextView

	private Context context ;

	/**
	 * Book manage by the adapter
	 */
	public Book book ;
	
	/**
	 * 
	 * @param context
	 * @param book
	 * @param myActivity
	 */
	public BookAdapter(Context context, Book book) {
		this.context = context ;
		this.book = book ;
	}

	/**
	 * @return book
	 */
	public Book getBook() {
		return book;
	}

	/**
	 * 
	 * @param book new book
	 */
	public void setBook(Book book) {
		this.book = book ;
	}


	/**
	 * 	
	 */
	@Override
	public int getCount() {
		return Book.getNumberAttributes() ;
	}

	/**
	 * 
	 */
	@Override
	public Object getItem(int position) {
		return book.getAttribute(position) ;
	}

	/**
	 * Name of the attribute
	 * @param position position in attributes
	 * @return @string/description based on the position of the attribute
	 */
	public String getItemName(int position) {
		int val = Book.getAttributeName(position) ;
		if (val < 0) {
			return "Unknown, should never appear, please contact Frederic.Drouhin@gmail.com";
		}
		return context.getString(val);
	}

	@Override
	public long getItemId(int position) {
		return position ;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// convertView null ?
		ViewHolder holder = null ;

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService
					(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(com.fd.bookmanagement.R.layout.bookadapter_layout, null);

			holder = new ViewHolder() ;
			holder.item = (TextView) convertView.findViewById(com.fd.bookmanagement.R.id.bookItem);
			holder.subItem= (TextView) convertView.findViewById(com.fd.bookmanagement.R.id.subBookItem);

			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder)convertView.getTag() ;
		}

		// Data to be displayed
		String item = "" ;
		if (getItem(position) != null) {
			item = getItem(position).toString(); 
			// not anymore needed since the max number of lines is specified in the TextView
			//.substring(0,getItem(position).toString().length() < BookAdapter.maxCharacterInLine-4 ? getItem(position).toString().length() : BookAdapter.maxCharacterInLine-4)	+ " ..." ;*/
		}
		String subItem = (String)getItemName(position);

		// Item and subItem
		holder.item.setText(item) ;
		holder.subItem.setText(subItem) ;

		return convertView;
	}

	/**
	 * Just to avoid finding again the views needed to display
	 */
	private static class ViewHolder {
		public TextView item ;
		public TextView subItem ;
	}
}
