package com.fd.bookmanagement;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Some useful methods
 * @author Frederic.Drouhin
 *
 */
public class Utils {

	/**
	 * The activity context
	 */
	public static Context context ;
	
	/**
	 * type of message
	 */
	public static final int WARNING = 0 ;
	public static final int ERROR = -1 ;
	public static final int INFORMATION = 1 ;
	public static final int QUESTION = 2 ;

	/**
	 * Button to be set in dialog
	 */
	public static final int YESNO = 100 ;
	public static final int OK = 101 ;
	public static final int YESNOCANCEL = 102 ;

	/**
	 * Answer of the dialog
	 */
	//public static final int OK = 101 ;
	public static final int YES = 201 ;
	public static final int NO = 201 ;
	public static final int CANCEL = 202 ;

	/**
	 * Button clicked
	 */
	private static int clickedButton = -1000 ;

	/**
	 * 
	 * @param context
	 */
	public Utils (Context context) {
		Utils.context = context ;
	}

	/**
	 * Display an AlertDialog
	 */
	public static void alertDialog (String title) {

		alertDialog(title,INFORMATION,OK);
	}

	/**
	 * 
	 * @param title
	 * @param boutons
	 */
	public static void alertDialog (String title, int boutons) {
		alertDialog(title,INFORMATION,boutons);
	}

	/**
	 * 
	 * @param title
	 * @param type
	 * @param boutons
	 */
	public static void alertDialog (String title, int type, int boutons) {

		if (context != null) {

			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setCancelable(true);
			//builder.setIcon(R.drawable.dialog_question);
			builder.setTitle(title);
			builder.setInverseBackgroundForced(true);
			switch (boutons) {
			case YESNOCANCEL:
			case YESNO:
				builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						clickedButton = YES ;
						dialog.dismiss();
					}
				});
				builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						clickedButton = NO ;
						dialog.dismiss();
					}
				});
				break ;
			case OK:
				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						clickedButton = OK ;
						dialog.dismiss();
					}
				});

			}
			AlertDialog alert = builder.create();
			alert.show();
		}
	}

	/**
	 * Which button pressed
	 * @return Which button pressed
	 */
	public static int getPressButton() {
		
		return clickedButton;
	}
}
