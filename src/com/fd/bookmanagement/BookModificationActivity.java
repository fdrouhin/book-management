package com.fd.bookmanagement;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.fd.bookmanagement.books.Book;


public class BookModificationActivity extends Activity {

	/**
	 * Book
	 */
	private Book book = null ;

	/**
	 * Edit texts
	 */
	private EditText editTitle;
	private EditText editSerie;
	private EditText editVolume;
	private EditText editAuthors;
	private EditText editPublisher;
	private EditText editIsbn;
	private EditText editBuyDate;
	private EditText editPrintDate;
	private EditText editCollection;
	private Spinner spinnerGender;

	/**
	 * onCreate
	 * Use the Book used by BookManagementActivity, we consider it as the book reference (can be null in case of new book)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bookmodification_layout);

		getTexts() ;

		// Set the gender
		// Create an ArrayAdapter using the string array and a default spinner layout
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.genders_array, android.R.layout.simple_spinner_item);

		// Specify the layout to use when the list of choices appears
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		// Apply the adapter to the spinner
		spinnerGender.setAdapter(adapter);


		if (BookManagementActivity.activityBook != null) {
			book = BookManagementActivity.activityBook ;

			// set the title etc.
			if (book.getTitle() != null) editTitle.setText(book.getTitle());
			if (book.getSerie() != null) editSerie.setText(book.getSerie());
			if (book.getVolume() != null) editVolume.setText(book.getVolume().toString());
			if (book.getAuthors() != null) editAuthors.setText(book.getAuthors());
			if (book.getPublisher() != null) editPublisher.setText(book.getPublisher());
			if (book.getIsbn() != null) editIsbn.setText(book.getIsbn());
			if (book.getBuyDate() != null) editBuyDate.setText(Book.dateToString(book.getBuyDate()));
			if (book.getPrintDate() != null) editPrintDate.setText(Book.dateToString(book.getPrintDate()));
			if (book.getCollection() != null) editCollection.setText(book.getCollection());

			// set the gender depending on the gender of the book
			if (book.getGender() != null) {

				int i = adapter.getPosition(book.getGender());

				if (i < 0) {
					//adapter.add(book.getGender());
					//i = adapter.getPosition(book.getGender());
				}

				if (i >= 0 && i <= adapter.getCount()) {
					spinnerGender.setSelection(i);
				}
			}			
		}
		else {
			BookManagementActivity.activityBook = new Book("");
			book = BookManagementActivity.activityBook ;
		}
	}

	/**
	 * Retrieve the Text corresponding to each of content of a book
	 */
	private void getTexts() {

		editTitle = (EditText)findViewById(R.id.modify_title) ;
		editSerie = (EditText)findViewById(R.id.modify_serie);
		editVolume = (EditText)findViewById(R.id.modify_volume);
		editAuthors = (EditText)findViewById(R.id.modify_authors);
		editPublisher = (EditText)findViewById(R.id.modify_publisher);
		editIsbn = (EditText)findViewById(R.id.modify_ibsn);
		spinnerGender = (Spinner)findViewById(R.id.spinner_gender);
		editBuyDate = (EditText)findViewById(R.id.modify_buydate);
		editPrintDate = (EditText)findViewById(R.id.modify_printdate);
		editCollection = (EditText)findViewById(R.id.modify_collection);
	}

	/**
	 * Options menu - create a search widget following the requirements provided by http://developer.android.com/guide/topics/search/search-dialog.html#UsingSearchWidget
	 * @param menu options menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the options menu from XML
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.bookmodification_menu, menu);

		return true ;
	}

	/**
	 *  Item management from options menu
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.modify_ok:

			String val = null ;

			book.setTitle(editTitle.getText().toString());
			book.setSerie(editSerie.getText().toString());
			val = editVolume.getText().toString() ;
			if (!val.isEmpty()) {
				try {
					book.setVolume(val);
				} catch (NumberFormatException e) {
				}
			}
			book.setAuthors(editAuthors.getText().toString());
			book.setPublisher(editPublisher.getText().toString());
			book.setIsbn(editIsbn.getText().toString());
			val = editBuyDate.getText().toString() ;
			if (!val.isEmpty()) book.setBuyDate(val);
			val = editPrintDate.getText().toString() ;
			if (!val.isEmpty()) book.setPrintDate(editPrintDate.getText().toString());
			book.setCollection(editCollection.getText().toString());
			book.setGender(spinnerGender.getSelectedItem().toString());

			setResult(RESULT_OK);
			finish();
			return true;
		default:
			setResult(RESULT_CANCELED);
			finish() ;
			return false;
		}
	}
}
